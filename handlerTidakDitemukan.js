// handler error 400
const handlerTidakDitemukan = (req,res,next) =>{
    res.status(404).json({
        status : "Gagal",
        error : "Halaman tidak ditemukan"
    });
};


module.exports = handlerTidakDitemukan;
