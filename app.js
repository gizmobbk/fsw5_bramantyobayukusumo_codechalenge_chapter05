// * import module
const express = require('express');
const serverError = require('./ServerError');
const handlerTidakDitemukan = require('./handlerTidakDitemukan');
const router = require('./router');

// aktifkan module
const app = express();

// definisikan PORT
const PORT = 4000;

// menggunakan view engine ejs
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

// middleware router, serverError, handlerTidakDitemukan
app.use(router);
app.use(serverError);
app.use(handlerTidakDitemukan);

// jalankan server pada port
app.listen(PORT, () => {
    console.log(`Server berjalan pada port ${PORT}`);
});