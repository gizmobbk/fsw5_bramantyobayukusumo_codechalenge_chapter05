const express = require('express');
const app = express();
const router = express.Router();

// routing index
router.get('/', (req, res)=>{
    res.render('index');
});

// routing permainan
router.get('/aplikasi', (req, res)=>{
    res.render('permainan');
});

module.exports = router;