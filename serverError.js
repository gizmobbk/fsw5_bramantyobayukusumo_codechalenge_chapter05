// handler error 500
const serverError = (err,req,res,next) =>{
    res.status(500).json({
        status : "Gagal",
        error : err.message
    });
};

module.exports = serverError;